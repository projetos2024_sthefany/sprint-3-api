const connect = require("../db/connect");

module.exports = class SalaDeAulaController {
  static async createSalaDeAula(req, res) {
    const { capacidade, recurso, localizacao } = req.body;

    if (!capacidade || !recurso || !localizacao) {
      return res.status(400).json({ error: "Todos os campos devem ser preenchidos" });
    }

    const query = `INSERT INTO sala_de_aula (capacidade, recurso, localizacao) VALUES (?, ?, ?)`;
    const values = [capacidade, recurso, localizacao];

    try {
      connect.query(query, values, function (err) {
        if (err) {
          console.error(err);
          return res.status(500).json({ error: "Erro interno do servidor" });
        }
        return res.status(201).json({ message: "Sala de aula criada com sucesso" });
      });
    } catch (error) {
      console.error("Erro ao executar a consulta:", error);
      res.status(500).json({ error: "Erro interno do servidor" });
    }
  }

  static async getAllSalasDeAula(req, res) {
    const query = `SELECT * FROM sala_de_aula`;

    try {
      connect.query(query, function (err, results) {
        if (err) {
          console.error(err);
          return res.status(500).json({ error: "Erro interno do servidor" });
        }

        return res.status(200).json({ message: "Obtendo todas as salas de aula", salas_de_aula: results });
      });
    } catch (error) {
      console.error("Erro ao executar a consulta:", error);
      return res.status(500).json({ error: "Erro interno do servidor" });
    }
  }

  static async getSalaDeAulaById(req, res) {
    const id_numero_da_sala = req.params.id;
    const query = `SELECT * FROM sala_de_aula WHERE id_numero_da_sala = ?`;
    const values = [id_numero_da_sala];

    try {
      connect.query(query, values, function (err, results) {
        if (err) {
          console.error(err);
          return res.status(500).json({ error: "Erro interno do servidor" });
        }

        if (results.length === 0) {
          console.error("Erro ao executar a consulta:", err);
          return res.status(404).json({ error: "Sala de aula não encontrada" });
        }

        return res.status(200).json({ message: "Obtendo sala de aula com ID: " + id_numero_da_sala, sala_de_aula: results[0] });
      });
    } catch (error) {
      console.error("Erro ao executar a consulta:", error);
      return res.status(500).json({ error: "Erro interno do servidor" });
    }
  }

  static async updateSalaDeAula(req, res) {
    const id_numero_da_sala = req.params.id;
    const { capacidade, recurso, localizacao } = req.body;

    if (!capacidade || !recurso || !localizacao) {
      return res.status(400).json({ error: "Todos os campos devem ser preenchidos" });
    }

    const query = `UPDATE sala_de_aula SET capacidade = ?, recurso = ?, localizacao = ? WHERE id_numero_da_sala = ?`;
    const values = [capacidade, recurso, localizacao, id_numero_da_sala];

    try {
      connect.query(query, values, function (err, results) {
        if (err) {
          console.error(err);
          return res.status(500).json({ error: "Erro interno do servidor" });
        }

        if (results.affectedRows === 0) {
          return res.status(404).json({ error: "Sala de aula não encontrada" });
        }

        return res.status(200).json({ message: "Sala de aula atualizada com ID: " + id_numero_da_sala });
      });
    } catch (error) {
      console.error("Erro ao executar a consulta:", error);
      return res.status(500).json({ error: "Erro interno do servidor" });
    }
  }

  static async deleteSalaDeAula(req, res) {
    const id_numero_da_sala = req.params.id;
    const query = `DELETE FROM sala_de_aula WHERE id_numero_da_sala = ?`;
    const values = [id_numero_da_sala];

    try {
      connect.query(query, values, function (err, results) {
        if (err) {
          console.error(err);
          return res.status(500).json({ error: "Erro interno do servidor" });
        }

        if (results.affectedRows === 0) {
          return res.status(404).json({ error: "Sala de aula não encontrada" });
        }

        return res.status(200).json({ message: "Sala de aula excluída com ID: " + id_numero_da_sala });
      });
    } catch (error) {
      console.error("Erro ao executar a consulta:", error);
      return res.status(500).json({ error: "Erro interno do servidor" });
    }
  }
};