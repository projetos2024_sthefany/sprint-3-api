const router = require('express').Router()
const userController = require ('../controller/userController');
const salaDeAulaController = require ('../controller/salaDeAulaController');
const dbController = require ('../controller/dbController');
const ReservaController = require ('../controller/ReservaController')

// USUÁRIO
router.post('/loginUsuario/', userController.postLogin); // Rota para fazer login
router.post('/createUsuario/', userController.createUsuario); // Rota para criar usuário
router.put('/updateUsuario/:id_aluno', userController.updateUser);
router.delete('/deleteUsuario/:id_aluno', userController.deleteUser);
router.get('/getUsuario/', userController.getAllUsuarios); // Rota para obter todos os usuários
router.get('/getUsuario/:id_aluno', userController.getUsuarioById); // Rota para obter um usuário pelo ID

//SALA DE AULA
router.post('/createSalaDeAula', salaDeAulaController.createSalaDeAula);
router.get('/getAllSchedules', salaDeAulaController.getAllSalasDeAula);
router.get('/getSalaDeAulaById/:id', salaDeAulaController.getSalaDeAulaById);
router.put('/updateSalaDeAula/:id', salaDeAulaController.updateSalaDeAula);
router.delete('/deleteSalaDeAula/:id', salaDeAulaController.deleteSalaDeAula);

//API COM BANCO DE DADOS,ROTA PARA CONSULTAR A TABELAS DO BANCO
router.get('/tables/',dbController.getNameTables)
router.get('/tablesdescriptions/', dbController.getTablesDescription); //rota para consulta das descrições da tabela do banco 



// Rotas para reservas
router.post('/classrooms', ReservaController.createClassroom);
// Rota para obter todas as salas (GET)
router.get('/classrooms', ReservaController.getAllClassrooms);
// Rota para obter uma sala específica por ID (GET)
router.get('/classrooms/:id', ReservaController.getClassroomById);
// Rota para atualizar uma sala (PUT)
router.put('/classrooms/:id', ReservaController.updateClassroom);
// Rota para excluir uma sala (DELETE)
router.delete('/classrooms/:id', ReservaController.deleteClassroom);





module.exports = router;